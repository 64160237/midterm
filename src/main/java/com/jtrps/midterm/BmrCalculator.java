package com.jtrps.midterm;

public class BmrCalculator {
    private String name;
    private String gender;
    private double age;
    private double weight;
    private double height;

    public BmrCalculator(String name, String gender, double age, double weight, double height) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public double calBmr() {
        double calBmr = 0;
        if (gender == "male") {
            calBmr = 66 + (13.7 *weight) + (5 *height) - (6.8 * age);
        } if (gender == "female") {
            calBmr = 665 + (9.6 *weight) + (1.8 *height) - (4.7 *age);
        }
        return calBmr;
    }

    public void printBmr() {
        System.out.println(name+ " BMR: " + calBmr());
    }
}
