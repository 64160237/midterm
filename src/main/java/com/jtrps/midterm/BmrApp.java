package com.jtrps.midterm;

public class BmrApp {
    public static void main(String[] args) {
        BmrCalculator alex = new BmrCalculator("Alex", "male", 20, 56, 175);
        BmrCalculator jasmine = new BmrCalculator("Jasmine", "female", 21, 61, 162);
        BmrCalculator lena = new BmrCalculator("Lena", "female", 32, 65, 165);
        alex.printBmr();
        jasmine.printBmr();
        lena.printBmr();
    }
}
