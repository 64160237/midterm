package com.jtrps.midterm;

public class BmiCalculator {
    private String name;
    private double weight;
    private double height;

    public BmiCalculator(String name, double weight, double height) {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public double heightMPow2() {
        double heightM = Math.pow(height/100, 2);
        return heightM;
    }

    public double calBmi() {
        double calBmi = weight/(heightMPow2());
        return calBmi;
    }

    public void printBmi() {
        System.out.println(name+ " BMI: " + calBmi());
    }
}
