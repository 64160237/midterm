package com.jtrps.midterm;

public class BmiApp {
    public static void main(String[] args) {
            BmiCalculator volley = new BmiCalculator("Volley", 55, 170);
            BmiCalculator tuu = new BmiCalculator("Tuu", 65, 160);
            BmiCalculator pom = new BmiCalculator("Pom", 80, 150);
            volley.printBmi();
            tuu.printBmi();
            pom.printBmi();
    }
}
